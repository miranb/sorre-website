{
  description = "My website hosting random thoughts";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    pre-commit-hooks-nix = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    no-style-please = {
      url = "gitlab:atgumx/no-style-please";
      flake = false;
    };
  };

  outputs =
    inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.treefmt-nix.flakeModule
        inputs.pre-commit-hooks-nix.flakeModule
      ];
      systems = [ "x86_64-linux" ];
      perSystem =
        { config, pkgs, ... }:
        let
          themeName = ((builtins.fromTOML (builtins.readFile "${inputs.no-style-please}/theme.toml")).name);
        in
        {
          packages = {
            default = config.packages.website;
            website = pkgs.stdenv.mkDerivation {
              pname = "sorre-website";
              version = "2025-01-21";
              src = ./.;
              nativeBuildInputs = [ pkgs.zola ];
              configurePhase = ''
                mkdir -p "themes/${themeName}"
                cp -r ${inputs.no-style-please}/* "themes/${themeName}"
              '';
              buildPhase = "zola build";
              installPhase = "cp -r public $out";
            };
          };

          devShells.default =
            let
              stdenvMinimal = pkgs.stdenvNoCC.override {
                cc = null;
                preHook = "";
                allowedRequisites = null;
                initialPath = pkgs.lib.filter (
                  a: pkgs.lib.hasPrefix "coreutils" a.name
                ) pkgs.stdenvNoCC.initialPath;
                extraNativeBuildInputs = [ ];
              };
              minimalMkShell = pkgs.mkShell.override { stdenv = stdenvMinimal; };
            in
            minimalMkShell {
              packages = with pkgs; [ zola ] ++ builtins.attrValues config.treefmt.build.programs;
              shellHook = ''
                mkdir -p themes
                ln -sn "${inputs.no-style-please}" "themes/${themeName}"
              '';
            };

          treefmt = {
            projectRootFile = "flake.nix";
            programs = {
              nixfmt.enable = true;
            };
          };

          pre-commit = {
            check.enable = true;
            settings.hooks = {
              treefmt.package = config.treefmt.build.wrapper;
              treefmt.enable = true;
              deadnix.enable = true;
            };
          };
        };
    };
}
