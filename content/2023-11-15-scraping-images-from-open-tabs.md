+++
title = "Scraping images from open tabs"
date = "2023-11-15"
updated = "2023-11-15"
+++
```zsh
cat export.json \
  | jaq --raw-output '..["url"]? | strings' \
  | rg --fixed-strings "wallhaven.cc/w" \
  | gallery-dl --input-file -
```
I exported my current session to json with tab session manager. This command
takes that json export (referred to as `export.json`), finds all the urls
that contain `wallhaven.cc/w` (i.e. all urls that are wallhaven images) and
passes them to gallery-dl. Gallery-dl then neatly downloads all of those images
in their original quality.

This workflow could be made better by creating a version of tab session manager
that exports in a simpler (or more configurable format), and I could also turn
this into a script that takes the wanted website formats as an argument.
Invocation of the script might look something like: 
```zsh
tabsession-dl --format "wallhaven.cc/w" --format "reddit.com/r" \
  --input-file export.json
```
